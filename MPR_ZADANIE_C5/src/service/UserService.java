package service;

import java.util.Comparator;
import domain.Person;
import domain.Role;
import domain.User;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class UserService {

	
    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
        return users.stream().filter(
				user -> user.getPersonDetails().getAddresses() != null &&
						user.getPersonDetails().getAddresses().size() > 1
		).collect(Collectors.toList());
    }

    public static Person findOldestPerson(List<User> users) {
        return users.stream().filter(
				user -> user.getPersonDetails() != null
		).max(
				Comparator.comparing(
						user -> user.getPersonDetails().getAge()
				)
		).get().getPersonDetails();
    }

    public static User findUserWithLongestUsername(List<User> users) {
        return users.stream().filter(
				user -> user.getName() != null
		).max(
				Comparator.comparing(
						user -> user.getName().length()
				)
		).get();
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {
        return users.stream().filter(
				user -> user.getPersonDetails() != null &&
						user.getPersonDetails().getName() != null &&
						user.getPersonDetails().getSurname() != null
		).map(
				user -> user.getPersonDetails().getName() + " " + user.getPersonDetails().getSurname()
		).collect(Collectors.joining(","));
    }

    public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {
        return users.stream().filter(
				user -> user.getPersonDetails() != null &&
						user.getName() != null &&
						user.getPersonDetails().getRole() != null &&
						user.getPersonDetails().getRole().getPermissions() != null &&
						user.getName().toLowerCase().charAt(0) == 'a'
		).flatMap(
				user -> user.getPersonDetails().getRole().getPermissions().stream().map(
						permission -> permission.getName()
				)
		).sorted(
				(permission1, permission2) -> permission1.compareTo(permission2)
		).collect(Collectors.toList());
    }

    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
        users.stream().filter(
				user -> user.getPersonDetails() != null &&
						user.getPersonDetails().getSurname() != null &&
						user.getPersonDetails().getSurname().toLowerCase().charAt(0) == 's' &&
						user.getPersonDetails().getRole() != null &&
						user.getPersonDetails().getRole().getPermissions() != null
		).flatMap(
				user -> user.getPersonDetails().getRole().getPermissions().stream().filter(
						permission -> permission.getName() != null
				).map(
						permission -> permission.getName().toUpperCase()
				)
		).forEach(
				permission -> System.out.println(permission)
		);
    }

    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {
		return users.stream().filter(
				user -> user.getPersonDetails() != null &&
						user.getPersonDetails().getRole() != null
		).collect(Collectors.groupingBy(
				user -> user.getPersonDetails().getRole()
		));
    }

    public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
		return users.stream().filter(
				user -> user.getPersonDetails() != null &&
						user.getPersonDetails().getAge() > 0
		).collect(Collectors.groupingBy(
				user -> user.getPersonDetails().getAge() >= 18
		));
    }
}